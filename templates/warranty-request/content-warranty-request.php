<?php
if (isset($_GET['action']) && $_GET['action'] == 'reset-password') {
    get_template_part('templates/warranty-request/content', 'reset-password');
} else if (! is_user_logged_in()) {
    get_template_part('templates/warranty-request/content', 'login');
}
?>

<div class="header__spacer"></div>

<div class="page-content page-content--grey-bg">
    <div class="container">
        <div class="warranty-request">
            <div class="warranty-request__info">
                <?php 
                if (have_rows('sections')) :
                    while (have_rows('sections')) : the_row();
                        ?>
                        <div class="warranty-request__section">
                            <h3 class="section__heading">
                                <span><?= get_sub_field('heading'); ?></span>
                                <div class="section__heading__border"></div>
                            </h3>

                            <div class="warranty-request__copy"><?= get_sub_field('copy'); ?></div>

                            <?php if (have_rows('downloads')) : ?>
                                <div class="warranty-request__downloads">
                                    <?php
                                    while (have_rows('downloads')) : the_row();
                                        $file = get_sub_field('file');
                                        ?>
                                        <a class="ui-button ui-button--secondary ui-button--icon" href="<?= $file['url']; ?>" target="_blank">
                                            <i class="material-icons">file_download</i>
                                            <span><?= $file['title']; ?></span>
                                        </a>
                                        <?php
                                    endwhile;
                                    ?>
                                </div>
                            <?php endif; ?>
                        </div>
                        <?php 
                    endwhile; 
                endif; 
                ?>
            </div>

            <div class="warranty-request__form">
                <?= get_field('form'); ?>
            </div>
        </div>
    </div>
</div>
