<?php
if (isset($_GET['key']) && isset($_GET['login'])) {
    $user = check_password_reset_key(wp_unslash($_GET['key']), wp_unslash($_GET['login']));
} else {
    $user = new WP_Error('invalid_key', __('Invalid key'));
}
?>

<div class="login">
    <form class="login__form" id="reset-password-form" method="post">
        <?php if (! is_wp_error($user)) { ?>
            <div class="login__description">Use the form below to reset your password.</div>

            <div class="login__feedback"></div>

            <fieldset>
                <label for="new-password">New password</label>
                <input type="password" name="new_password" id="new-password" placeholder="New password" />
            </fieldset>

            <fieldset>
                <label for="confirm-password">Confirm password</label>
                <input type="password" name="confirm_password" id="confirm-password" placeholder="Confirm password" />
            </fieldset>

            <button type="submit" class="login__form__submit ui-button ui-button--primary">Reset My Password</button>

            <input type="hidden" name="user_id" value="<?= $user->ID; ?>" />

            <?php wp_nonce_field('reset_password', 'reset_password_nonce'); ?>
        <?php } else { ?>
            <div class="login__description">
                <h3><?= $user->get_error_message(); ?></h3>
                <p>Sorry, we can't reset your password because your key is invalid.</p>
            </div>

            <a class="login__form__submit ui-button ui-button--primary" href="<?= get_permalink(); ?>">Go Back</a>
        <?php } ?>
    </form>
</div>
