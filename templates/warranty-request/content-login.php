<div class="login">
    <form class="login__form" id="login-form" method="post">
        <div class="login__description">Please log in below to view this page.</div>

        <div id="feedback-error"></div>

        <fieldset>
            <label for="email">Email</label>
            <input type="email" name="email" id="email" placeholder="Email" />
        </fieldset>

        <fieldset>
            <label for="password">Password</label>
            <input type="password" name="password" id="password" placeholder="Password" />
        </fieldset>

        <button type="submit" class="login__form__submit ui-button ui-button--primary ui-button--loader">Log in</button>

        <a id="send-password-reset-link-form-link" class="login__footer-link">Forgot your password?</a>

        <input type="hidden" name="redirect_to" value="<?= get_permalink(); ?>" />

        <?php wp_nonce_field('login_user', 'login_user_nonce'); ?>
    </form>

    <form class="login__form" id="send-password-reset-link-form" method="post">
        <div class="login__description">Forgot your password? No problem, just enter your email below and we'll send you a link to reset your password.</div>

        <div class="login__feedback"></div>

        <fieldset>
            <label for="email">Email</label>
            <input type="email" name="email" id="email" placeholder="Email" />
        </fieldset>

        <button type="submit" class="login__form__submit ui-button ui-button--primary">Reset My Password</button>

        <a id="login-form-link" class="login__footer-link">Just kidding, take me back.</a>

        <?php wp_nonce_field('send_password_reset_link', 'send_password_reset_link_nonce'); ?>
    </form>
</div>
