<?php
if (get_field('sq_ft')) {
    ?>
    <div class="cards__attribute">
        <i class="material-icons">view_compact</i>
        <?= get_field('sq_ft') . ' Sq. Ft.'; ?>
    </div>
    <?php
}

if (get_field('bedrooms')) {
    ?>
    <div class="cards__attribute">
        <i class="material-icons">hotel</i>
        <?= get_field('bedrooms') . ' Beds'; ?>
    </div>
    <?php
}

if (get_field('bathrooms')) {
    ?>
    <div class="cards__attribute">
        <i class="material-icons">wc</i>
        <?= get_field('bathrooms') . ' Baths'; ?>
    </div>
    <?php
}

if (get_field('garages')) {
    ?>
    <div class="cards__attribute">
        <i class="material-icons">drive_eta</i>
        <?= get_field('garages') . ' Garages'; ?>
    </div>
    <?php
}
