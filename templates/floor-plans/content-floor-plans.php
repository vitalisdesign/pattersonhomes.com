<div class="header__spacer"></div>

<div class="floor-plans cards page-content page-content--grid-with-filter">
    <div class="container">
        <form class="floor-plans__filter section__filter ui-button-group">
            <input type="radio" name="home_type" value="all" id="all" class="ui-button--radio" checked="checked" />
            <label for="all" class="ui-button ui-button--default">All</label>

            <?php
            $home_type_terms = get_terms([
                'taxonomy' => 'home_type',
                'hide_empty' => false
            ]);

            foreach ($home_type_terms as $home_type_term) {
                ?>
                <input type="radio" name="home_type" value="<?= $home_type_term->slug; ?>" id="<?= $home_type_term->slug; ?>" class="ui-button--radio" />
                <label for="<?= $home_type_term->slug; ?>" class="ui-button ui-button--default"><?= $home_type_term->name; ?></label>
                <?php
            }
            ?>
        </form>

        <div class="floor-plans__list cards__list">
            <?php
            $query = new WP_Query([
                'post_type' => 'floor_plan', 
                'posts_per_page' => -1,
                'orderby' => 'meta_value_num',
                'meta_key' => 'sq_ft'
            ]);
            if ($query->have_posts()) :
                while ($query->have_posts()) : $query->the_post();
                    get_template_part('templates/floor-plans/content', 'floor-plans-item');
                endwhile; wp_reset_query();
            else :
                get_template_part('templates/content', 'no-results');
            endif;

            get_template_part('templates/loader');
            ?>
        </div>
    </div>
</div>
