<?php
$featuredImage = wp_get_attachment_image_src(get_post_thumbnail_id(), 'max');
?>

<div class="floor-plan__header page-header__cover" style="background-image: url(<?= $featuredImage[0]; ?>);">
    <div class="floor-plan__header__content">
        <div class="container">
            <h1 class="floor-plan__header__title"><?php the_title(); ?></h1>

            <div class="floor-plan__header__attributes">
                <?php get_template_part('templates/content', 'home-attributes'); ?>
            </div>
        </div>
    </div>
</div>

<?php
$sectionsCount = 0;
if (get_field('floor_plans')) {
    $sectionsCount++;
}
if (get_field('images')) {
    $sectionsCount++;
}
if (get_field('flyer_pdf')) {
    $sectionsCount++;
}

if ($sectionsCount >= 2) {
    ?>
    <div class="page-nav">
        <div class="page-nav__list">
            <?php if (get_field('floor_plans')) { ?>
                <a class="page-nav__item" href="#floor-plans">
                    <i class="material-icons">view_compact</i>
                    <?= __('Floor Plans'); ?>
                </a>
            <?php } ?>

            <?php if (get_field('images')) { ?>
                <a class="page-nav__item" href="#images">
                    <i class="material-icons">photo_library</i>
                    <?php printf(__('Photos from %s'), get_the_title()); ?>
                </a>
            <?php } ?>

            <?php if (get_field('flyer_pdf')) { ?>
                <a class="page-nav__item" href="#flyer">
                    <i class="material-icons">picture_as_pdf</i>
                    <?= __('Flyer'); ?>
                </a>
            <?php } ?>
        </div>
    </div>
    <div class="page-nav__spacer"></div>
    <?php
}
?>

<div class="page-content page-content--grey-bg">
    <div class="container">
        <?php if (get_field('floor_plans')) { ?>
            <div class="floor-plan__floor-plans__container" id="floor-plans">
                <div class="floor-plan__floor-plans">
                    <?php
                    foreach (get_field('floor_plans') as $floor_plan) {
                        $floor_plan_src_max = wp_get_attachment_image_src($floor_plan['ID'], 'max');
                        $size = $floor_plan_src_max[1] . 'x' . $floor_plan_src_max[2];
                        ?>
                        <a href="<?= wp_get_attachment_image_src($floor_plan['ID'], 'max')[0]; ?>" data-size="<?= $size; ?>">
                            <img src="<?= wp_get_attachment_image_src($floor_plan['ID'], 'large')[0]; ?>" alt="<?= get_the_title(); ?>" />
                        </a>
                        <?php
                    }
                    ?>
                </div>

                <?php get_template_part('templates/photoswipe'); ?>

                <div class="floor-plan__floor-plans__attributes">
                    <?php get_template_part('templates/content', 'home-attributes'); ?>
                </div>
            </div>
        <?php } ?>

        <?php if (get_field('images')) { ?>
            <div class="floor-plan__slider__container" id="images">
                <div class="section__heading--centered">
                    <h3 class="section__heading">
                        <span><?php printf(__('Photos from %s'), get_the_title()); ?></span>
                        <div class="section__heading__border"></div>
                    </h3>
                </div>

                <div class="floor-plan__slider">
                    <?php
                    foreach (get_field('images') as $image) {
                        $image_src_max = wp_get_attachment_image_src($image['ID'], 'max');
                        $size = $image_src_max[1] . 'x' . $image_src_max[2];
                        ?>
                        <a href="<?= wp_get_attachment_image_src($image['ID'], 'max')[0]; ?>" data-size="<?= $size; ?>">
                            <img src="<?= wp_get_attachment_image_src($image['ID'], 'large')[0]; ?>" alt="<?= get_the_title(); ?>" />
                        </a>
                        <?php
                    }
                    ?>
                </div>
            </div>
        <?php } ?>

        <?php if (get_field('flyer_pdf') || get_field('availability')) { ?>
            <div class="floor-plan__extras">
                <?php if (get_field('flyer_pdf')) { ?>
                    <div class="floor-plan__flyer" id="flyer">
                        <h3 class="section__heading">
                            <span>Download Flyer</span>
                            <div class="section__heading__border"></div>
                        </h3>

                        <?php
                        $flyer_preview = wp_get_attachment_image_src(get_field('flyer_preview')['ID'], 'large');
                        ?>
                        <a class="floor-plan__flyer__preview" href="<?= get_field('flyer_pdf')['url']; ?>" target="_blank" style="background-image: url(<?= $flyer_preview[0]; ?>);">
                            <i class="floor-plan__flyer__icon-download material-icons">file_download</i>

                            <?php if (! $flyer_preview) { ?>
                                <i class="floor-plan__flyer__icon-pdf material-icons">picture_as_pdf</i>
                            <?php } ?>
                        </a>
                    </div>
                <?php } ?>

                <?php if (get_field('availability')) { ?>
                    <div class="floor-plan__availability">
                        <h3 class="section__heading">
                            <span>Floor Plan Available In...</span>
                            <div class="section__heading__border"></div>
                        </h3>

                        <div class="floor-plan__availability__list">
                            <?php
                            global $post;
                            foreach (get_field('availability') as $post) {
                                if (get_post_status($post) != 'publish') {
                                    continue;
                                }
                                setup_postdata($post);
                                $featuredImage = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium');
                                ?>
                                <a class="floor-plan__availability__item"  href="<?= get_permalink(); ?>">
                                    <div class="floor-plan__availability__top" style="background-image: url(<?= $featuredImage[0]; ?>);"></div>

                                    <div class="floor-plan__availability__bottom"><?= get_the_title(); ?></div>
                                </a>
                                <?php
                            }
                            wp_reset_postdata();
                            ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</div>
