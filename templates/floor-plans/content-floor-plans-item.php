<a class="cards__item" href="<?= get_permalink(); ?>">
    <?php
    $featuredImage = wp_get_attachment_image_src(get_post_thumbnail_id(), 'large');
    ?>
    <div class="cards__top" style="background-image: url(<?= $featuredImage[0]; ?>);">
        <?php if (get_the_title()) { ?>
            <div class="cards__accent"><?= get_the_title(); ?></div>
        <?php } ?>
    </div>

    <div class="cards__bottom">
        <div class="cards__attributes">
            <?php
            get_template_part('templates/content', 'home-attributes');
            ?>
        </div>
    </div>
</a>
