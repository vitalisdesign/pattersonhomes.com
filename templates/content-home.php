<div class="home__slider__container">
    <div class="home__slider">
        <?php
        foreach (get_field('slider') as $image) {
            echo wp_get_attachment_image($image['ID'], 'full');
        }
        ?>
    </div>

    <div class="page-header__banner">
        <div class="page-header__banner__links">
            <div class="page-header__banner__heading">Where would you like to live?</div>

            <?php
            $locations_page = get_page_by_path('locations');
            ?>
            <a class="ui-button ui-button--primary ui-button--banner" href="<?= get_permalink($locations_page); ?>">
                <?= get_the_title($locations_page); ?>
            </a>

            <?php
            $quick_move_ins_page = get_page_by_path('quick-move-ins');
            ?>
            <a class="ui-button ui-button--primary ui-button--banner" href="<?= get_permalink($quick_move_ins_page); ?>">
                <?= get_the_title($quick_move_ins_page); ?>
            </a>
        </div>
    </div>
</div>

<div class="page-content page-content--grey-bg">
    <div class="container">
        <div class="home__links__list">
            <?php
            if (have_rows('links')) :
                while (have_rows('links')) : the_row();
                    ?>
                    <div class="home__links__item home__links__<?= get_row_layout(); ?>">
                        <?php
                        if (get_row_layout() == 'locations') {
                            $image_src = wp_get_attachment_image_src(get_sub_field('image')['ID'], 'large');
                            ?>
                            <a href="<?= get_sub_field('link'); ?>" class="home__links__<?= get_row_layout(); ?>__image" style="background-image: url(<?= $image_src[0]; ?>);"></a>

                            <a class="home__links__link ui-button ui-button--secondary" href="<?= get_sub_field('link'); ?>"><?= get_sub_field('link_text'); ?></a>
                            <?php
                        } else if (get_row_layout() == 'quick_move_ins') {
                            $query = new WP_Query([
                                'post_type' => 'quick_move_in', 
                                'posts_per_page' => 1,
                                'orderby' => 'rand'
                            ]);
                            if ($query->have_posts()) :
                                while ($query->have_posts()) : $query->the_post();
                                    get_template_part('templates/quick-move-ins/content', 'quick-move-ins-item');
                                endwhile; wp_reset_query();
                            endif;
                            ?>

                            <a class="home__links__link ui-button ui-button--secondary" href="<?= get_sub_field('link'); ?>"><?= get_sub_field('link_text'); ?></a>
                            <?php
                        } else if (get_row_layout() == 'gallery') {
                            ?>
                            <a href="<?= get_sub_field('link'); ?>" class="home__links__<?= get_row_layout(); ?>__images">
                                <?php
                                foreach (get_sub_field('images') as $image) {
                                    $image_src = wp_get_attachment_image_src($image['ID'], 'medium');
                                    ?>
                                    <div style="background-image: url(<?= $image_src[0]; ?>);"></div>
                                    <?php
                                }
                                ?>
                            </a>

                            <a class="home__links__link ui-button ui-button--secondary" href="<?= get_sub_field('link'); ?>"><?= get_sub_field('link_text'); ?></a>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                endwhile;
            endif;
            ?>
        </div>
    </div>
</div>

<div class="page-content page-content--light-grey-bg">
    <div class="container">
        <?php
        if (have_rows('content')) :
            while (have_rows('content')) : the_row();
                ?>
                <div class="section section--<?= get_row_layout(); ?>">
                    <?php
                    if (get_row_layout() == 'copy_image_align_left' || get_row_layout() == 'copy_image_align_right') {
                        ?>
                        <div class="section__text">
                            <?php if (get_sub_field('heading')) { ?>
                                <h2 class="section__heading">
                                    <span><?= get_sub_field('heading'); ?></span>
                                    <div class="section__heading__border"></div>
                                </h2>
                            <?php } ?>

                            <?php if (get_sub_field('copy')) { ?>
                                <div class="section__copy"><?= get_sub_field('copy'); ?></div>
                            <?php } ?>

                            <?php if (get_sub_field('link')) { ?>
                                <a class="section__link ui-button ui-button--secondary" href="<?= get_sub_field('link'); ?>"><?= get_sub_field('link_text'); ?></a>
                            <?php } ?>
                        </div>

                        <?php
                        $image = wp_get_attachment_image_src(get_sub_field('image')['ID'], 'large');
                        if ($image) {
                            ?>
                            <div class="section__image" style="background-image: url(<?= $image['0']; ?>);"></div>
                            <?php
                        }
                    }
                    ?>
                </div>
                <?php
            endwhile;
        endif;
        ?>
    </div>
</div>
