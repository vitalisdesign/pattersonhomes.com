<div class="page-content page-content--grey-bg">
    <div class="container">
        <?php
        if (have_rows('sections')) :
            while (have_rows('sections')) : the_row();
                ?>
                <div class="section section--<?= get_row_layout(); ?>">
                    <?php
                    if (get_row_layout() == 'copy_image_align_left' || get_row_layout() == 'copy_image_align_right') {
                        ?>
                        <div class="section__text">
                            <?php if (get_sub_field('heading')) { ?>
                                <h2 class="section__heading">
                                    <span><?= get_sub_field('heading'); ?></span>
                                    <div class="section__heading__border"></div>
                                </h2>
                            <?php } ?>

                            <?php if (get_sub_field('copy')) { ?>
                                <div class="section__copy"><?= get_sub_field('copy'); ?></div>
                            <?php } ?>
                        </div>

                        <?php
                        $image = wp_get_attachment_image_src(get_sub_field('image')['ID'], 'large');
                        if ($image) {
                            ?>
                            <div class="section__image" style="background-image: url(<?= $image['0']; ?>);"></div>
                            <?php
                        }
                    } else if (get_row_layout() == 'copy_centered') {
                        ?>
                        <?php if (get_sub_field('heading')) { ?>
                            <div class="section__heading--centered">
                                <h3 class="section__heading">
                                    <span><?= get_sub_field('heading'); ?></span>
                                    <div class="section__heading__border"></div>
                                </h3>
                            </div>
                        <?php } ?>

                        <?php if (get_sub_field('copy')) { ?>
                            <div class="section__copy"><?= get_sub_field('copy'); ?></div>
                        <?php }
                    }
                    ?>
                </div>
                <?php
            endwhile;
        endif;
        ?>
    </div>
</div>
