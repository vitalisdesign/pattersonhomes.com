<?php
$featuredImage = wp_get_attachment_image_src(get_post_thumbnail_id(), 'max');
$address = get_field('address')['address'];
$directionsLink = 'https://www.google.com/maps/dir//' . $address;

if (get_field('address_label')) {
    $addressLabel = get_field('address_label');
} else {
    $addressLabel = preg_replace('/,/', '<br />', $address, 1);
}
?>

<div class="location__header page-header__cover" style="background-image: url(<?= $featuredImage[0]; ?>);">
    <div class="page-header__cover-filter">
        <div class="location__header__content">
            <h1 class="location__header__title"><?php the_title(); ?></h1>
            <a class="location__header__address" href="<?= $directionsLink; ?>" target="_blank"><?= $addressLabel; ?></a>
        </div>
    </div>
</div>

<div class="page-content page-content--grey-bg">
    <div class="container">
        <div class="location__info">
            <div class="location__downloads">
                <?php
                if (have_rows('downloads')):
                    while (have_rows('downloads')) : the_row();
                        $file = get_sub_field('file');
                        ?>
                        <a class="ui-button ui-button--primary ui-button--icon" href="<?= $file['url']; ?>" target="_blank">
                            <i class="material-icons">file_download</i>
                            <span><?= $file['title']; ?></span>
                        </a>
                        <?php
                    endwhile;
                endif;
                ?>
            </div>

            <div class="location__description"><?= get_field('description'); ?></div>
        </div>

        <?php
        $linked_floor_plans = [];
        $current_location = $post;

        $query = new WP_Query([
            'post_type' => 'floor_plan',
            'posts_per_page' => -1,
            'orderby' => 'meta_value_num',
            'meta_key' => 'sq_ft'
        ]);
        if ($query->have_posts()) :
            while ($query->have_posts()) : $query->the_post();
                if (get_field('availability')) {
                    foreach (get_field('availability') as $location) {
                        if ($location->post_name == $current_location->post_name) {
                            $linked_floor_plans[] = $post;
                        }
                    }
                }
            endwhile; wp_reset_query();
        endif;

        if ($linked_floor_plans) {
            ?>
            <div class="location__floor-plans">
                <h3 class="section__heading">
                    <span>Floor Plans Available In This Community...</span>
                    <div class="section__heading__border"></div>
                </h3>

                <div class="location__floor-plans__list">
                    <?php
                    global $post;
                    foreach ($linked_floor_plans as $post) {
                        setup_postdata($post);
                        $featuredImage = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium');
                        ?>
                        <a class="location__floor-plans__item"  href="<?= get_permalink(); ?>">
                            <div class="location__floor-plans__top" style="background-image: url(<?= $featuredImage[0]; ?>);"></div>

                            <div class="location__floor-plans__bottom"><?= get_the_title(); ?></div>
                        </a>
                        <?php
                    }
                    wp_reset_postdata();
                    ?>
                </div>
            </div>
            <?php
            }
        ?>
    </div>
</div>
