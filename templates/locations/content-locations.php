<div class="header__spacer"></div>

<div id="locations-map" class="map"></div>

<?php
$communitiesCount = wp_count_posts('location');
?>
<div class="locations__banner">Building in over <strong><?= $communitiesCount->publish; ?></strong> Utah valley communities</div>

<div class="locations__list page-content page-content--grey-bg">
    <div class="container">
        <div class="locations__list__sections-container">
            <?php
            $city_terms = get_terms([
                'taxonomy' => 'city'
            ]);

            $cities = [];
            foreach ($city_terms as $city_term) {
                ?>
                <div class="locations__list__section">
                    <h3 class="locations__list__heading"><?= $city_term->name; ?></h3>
                    <div class="locations__list__heading-border"></div>

                    <ul class="locations__list__cities">
                        <?php
                        $query = new WP_Query([
                            'post_type' => 'location', 
                            'posts_per_page' => -1,
                            'order_by' => 'menu_order',
                            'order' => 'ASC',
                            'tax_query' => [
                                [
                                    'taxonomy' => 'city',
                                    'terms' => $city_term->term_id,
                                ],
                            ]
                        ]);

                        while ($query->have_posts()) : $query->the_post();
                            ?>
                            <li><a href="<?= get_permalink(); ?>"><?= get_the_title(); ?></a></li>
                            <?php
                        endwhile; wp_reset_query();
                        ?>
                    </ul>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>
