<?php 
use Roots\Sage\Assets;
?>

<footer class="footer">
    <div class="container">
        <div class="footer__legal">
            <img class="footer__since-1938" src="<?= Assets\asset_path('images/since_1938.svg'); ?>" />

            <p>&copy; <?php printf('%s %s. %s', date('Y'), get_bloginfo('name'), __('All Rights Reserved')); ?>.</p>

            <p class="footer__credit">Website Created by <a href="https://www.vitalisdesign.com" target="_blank" title="Website Created by Vitalis Design">Vitalis Design</a>.</p>
        </div>

        <div class="footer__contact-links">
            <?php
            $address = get_field('contact__address', 'option')['address'];
            $placeLink = sprintf('https://www.google.com/maps/place/%s', urlencode($address));
            ?>
            <a href="<?= $placeLink; ?>" target="_blank">
                <i class="material-icons">location_on</i>
                <span><?= preg_replace('/,/', '<br />', $address, 1); ?></span>
            </a>

            <div class="footer__numbers">
                <i class="material-icons">phone</i>
                <div>
                    <a href="tel:<?= get_field('contact__phone_office', 'option'); ?>">Office: <?= get_field('contact__phone_office', 'option'); ?></a>
                    <a href="tel:<?= get_field('contact__phone_sales', 'option'); ?>">Sales: <?= get_field('contact__phone_sales', 'option'); ?></a>
                </div>
            </div>

            <a href="mailto:<?= get_field('contact__email', 'option'); ?>">
                <i class="material-icons">email</i>
                <span><?= get_field('contact__email', 'option'); ?></span>
            </a>
        </div>

        <div class="footer__social-media-links">
            <?php if (get_field('social_media__facebook_link', 'option')) { ?>
                <a href="<?= get_field('social_media__facebook_link', 'option'); ?>" target="_blank">
                    <img src="<?= Assets\asset_path('images/icons/facebook.svg'); ?>" />
                </a>
            <?php } ?>

            <?php if (get_field('social_media__instagram_link', 'option')) { ?>
                <a href="<?= get_field('social_media__instagram_link', 'option'); ?>" target="_blank">
                    <img src="<?= Assets\asset_path('images/icons/instagram.svg'); ?>" />
                </a>
            <?php } ?>

            <?php if (get_field('social_media__pinterest_link', 'option')) { ?>
                <a href="<?= get_field('social_media__pinterest_link', 'option'); ?>" target="_blank">
                    <img src="<?= Assets\asset_path('images/icons/pinterest.svg'); ?>" />
                </a>
            <?php } ?>
        </div>
    </div>
</footer>

<?php if (! is_page('contact-us')) { ?>
    <a class="contact-sticky" href="<?= get_permalink(get_page_by_path('contact-us')); ?>">
        <i class="material-icons">chat_bubble</i>
    </a>
<?php } ?>
