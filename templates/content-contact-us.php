<?php
$featuredImage = wp_get_attachment_image_src(get_post_thumbnail_id(), 'max');
?>

<div class="page-header__cover" style="background-image: url(<?= $featuredImage[0]; ?>);">
    <div class="page-header__banner">
        <div class="page-header__banner__links">
            <a class="ui-button ui-button--primary ui-button--icon ui-button--banner" href="mailto:<?= get_field('contact__email', 'option'); ?>">
                <i class="material-icons">email</i>
                <span><?= get_field('contact__email', 'option'); ?></span>
            </a>

            <a class="ui-button ui-button--primary ui-button--icon ui-button--banner" href="tel:<?= get_field('contact__phone_sales', 'option'); ?>">
                <i class="material-icons">phone</i>
                <span><?= get_field('contact__phone_sales', 'option'); ?></span>
            </a>

            <?php
            $warranty_request_page = get_page_by_path('warranty-request');
            ?>
            <a class="ui-button ui-button--primary ui-button--icon ui-button--banner" href="<?= get_permalink($warranty_request_page); ?>">
                <i class="material-icons">assignment</i>
                <span><?= get_the_title($warranty_request_page); ?></span>
            </a>
        </div>
    </div>
</div>

<div class="page-content page-content--grey-bg">
    <div class="container">
        <div class="contact-us__contact-info">
            <div class="contact-us__form">
                <h3 class="section__heading">
                    <span><?= get_field('form_heading'); ?></span>
                    <div class="section__heading__border"></div>
                </h3>

                <div class="contact-us__form__description"><?= get_field('form_description'); ?></div>

                <?= get_field('form'); ?>
            </div>

            <div class="contact-us__map">
                <iframe src="https://www.google.com/maps/embed/v1/place?key=<?= GOOGLE_API_KEY; ?>&q=<?= urlencode(get_field('contact__address', 'option')['address']); ?>" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>

        <?php if (have_rows('agents')) : ?>
            <div class="contact-us__agents">
                <h3 class="section__heading">
                    <span>Contact an Agent</span>
                    <div class="section__heading__border"></div>
                </h3>

                <div class="contact-us__agents__list">
                    <?php
                    while (have_rows('agents')) : the_row();
                        $headshot = wp_get_attachment_image_src(get_sub_field('headshot')['ID'], 'medium');
                        ?>
                        <div class="contact-us__agents__item">
                            <div class="contact-us__agents__headshot" style="background-image: url(<?= $headshot[0]; ?>);"></div>

                            <div>
                                <div class="contact-us__agents__name"><?= get_sub_field('name'); ?></div>

                                <a class="contact-us__agents__phone" href="tel:<?= get_sub_field('phone'); ?>"><?= get_sub_field('phone'); ?></a>
                                <a class="contact-us__agents__email" href="mailto:<?= get_sub_field('email'); ?>"><?= get_sub_field('email'); ?></a>
                            </div>
                        </div>
                        <?php
                    endwhile;
                    ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
