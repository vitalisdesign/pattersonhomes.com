<div class="header__spacer"></div>

<div class="quick-move-ins cards page-content page-content--grid-with-filter">
    <div class="container">
        <form class="quick-move-ins__filter section__filter ui-button-group">
            <select name="city" data-placeholder="City">
                <option></option>
                <?php
                $city_terms = get_terms([
                    'taxonomy' => 'city',
                    'hide_empty' => false
                ]);

                foreach ($city_terms as $city_term) {
                    ?>
                    <option value="<?= $city_term->slug; ?>"><?= $city_term->name; ?></option>
                    <?php
                }
                ?>
            </select>

            <select name="price" data-placeholder="Price">
                <option></option>
                <option value="0-250000">Under $250,000</option>
                <option value="250000-300000">$250,000 - $300,000</option>
                <option value="300000-350000">$300,000 - $350,000</option>
                <option value="350000-400000">$350,000 - $400,000</option>
                <option value="400000-1000000000">$400,000+</option>
            </select>

            <select name="home_type" data-placeholder="Home Type">
                <option></option>
                <?php
                $home_type_terms = get_terms([
                    'taxonomy' => 'home_type',
                    'hide_empty' => false
                ]);

                foreach ($home_type_terms as $home_type_term) {
                    ?>
                    <option value="<?= $home_type_term->slug; ?>"><?= $home_type_term->name; ?></option>
                    <?php
                }
                ?>
            </select>

            <a class="quick-move-ins__filter__clear ui-button ui-button--default ui-button--icon">
                <i class="material-icons">highlight_off</i>
                <span>Clear</span>
            </a>
        </form>

        <div class="quick-move-ins__list cards__list">
            <?php
            $query = new WP_Query([
                'post_type' => 'quick_move_in', 
                'posts_per_page' => -1
            ]);
            if ($query->have_posts()) :
                while ($query->have_posts()) : $query->the_post();
                    get_template_part('templates/quick-move-ins/content', 'quick-move-ins-item');
                endwhile; wp_reset_query();
            else :
                get_template_part('templates/quick-move-ins/content', 'no-results');
            endif;

            get_template_part('templates/loader');
            ?>
        </div>
    </div>
</div>
