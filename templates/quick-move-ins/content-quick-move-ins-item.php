<div class="cards__item">
    <?php
    $featuredImage = wp_get_attachment_image_src(get_post_thumbnail_id(), 'large');
    ?>
    <div class="cards__top" style="background-image: url(<?= $featuredImage[0]; ?>);">
        <?php if (get_field('price')) { ?>
            <div class="cards__accent">
                <?= get_field('price_status') ? '<strong>' . get_field('price_status') . '</strong>' . ' - ' : ''; ?>
                <?= '$' . number_format(get_field('price')); ?>
            </div>
        <?php } ?>

        <?php
        $address = get_field('address')['address'];
        $directionsLink = 'https://www.google.com/maps/dir//' . $address;

        if (get_field('address_label')) {
            $addressLabel = get_field('address_label');
        } else {
            $addressLabel = $address;
        }
        ?>
        <a class="cards__image-caption" href="<?= $directionsLink; ?>" target="_blank"><?= $addressLabel; ?></a>
    </div>

    <div class="cards__bottom">
        <div class="cards__attributes">
            <?php
            get_template_part('templates/content', 'home-attributes');

            if (get_field('floor_plan')) {
                $floor_plan = get_field('floor_plan');
                ?>
                <a class="cards__button ui-button ui-button--secondary" href="<?= get_permalink($floor_plan->ID); ?>">
                    <?= get_the_title($floor_plan->ID); ?>
                </a>
                <?php
            }
            ?>

            <?php if (get_field('link')) { ?>
                <a class="cards__link" href="<?= get_field('link'); ?>" target="_blank">
                    <i class="material-icons">open_in_new</i>
                    <span><?= get_field('link_text'); ?></span>
                </a>
            <?php } ?>
        </div>
    </div>
</div>
