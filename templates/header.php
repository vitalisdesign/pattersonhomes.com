<?php 
use Roots\Sage\Assets;
?>

<header class="header">
    <div class="header__inner-container container">
        <div class="header__logo-toggle-container">
            <a class="header__logo" href="<?= esc_url(home_url('/')); ?>">
                <img src="<?= Assets\asset_path('images/patterson_homes_logo.svg'); ?>" />
            </a>

            <a class="header__toggle">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </a>
        </div>

        <div class="header__nav-container">
            <nav class="header__nav">
                <?php
                if (has_nav_menu('primary_navigation')) :
                    wp_nav_menu(['theme_location' => 'primary_navigation', 'container' => '']);
                endif;
                ?>
            </nav>

            <div class="header__numbers">
                <a href="tel:<?= get_field('contact__phone_office', 'option'); ?>">Office: <?= get_field('contact__phone_office', 'option'); ?></a>
                <a href="tel:<?= get_field('contact__phone_sales', 'option'); ?>">Sales: <?= get_field('contact__phone_sales', 'option'); ?></a>
            </div>
        </div>
    </div>
</header>
