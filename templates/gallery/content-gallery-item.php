<?php
$image_src_max = wp_get_attachment_image_src($attachment_id, 'max');
$size = $image_src_max[1] . 'x' . $image_src_max[2];
?>

<a href="<?= wp_get_attachment_image_src($attachment_id, 'max')[0]; ?>" data-size="<?= $size; ?>">
    <img src="<?= wp_get_attachment_image_src($attachment_id, 'large')[0]; ?>" alt="<?= get_the_title(); ?>" />
</a>
