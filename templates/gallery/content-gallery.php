<?php
$offset = 10;
?>

<div class="header__spacer"></div>

<div class="gallery page-content page-content--grid-with-filter">
    <div class="container">
        <form class="gallery__filter section__filter ui-button-group">
            <input type="radio" name="gallery_tag" value="all" id="all" class="ui-button--radio" checked="checked" />
            <label for="all" class="ui-button ui-button--default">All</label>

            <?php
            $gallery_tags = get_terms([
                'taxonomy' => 'gallery_tag',
                'hide_empty' => false
            ]);

            foreach ($gallery_tags as $gallery_tag) {
                ?>
                <input type="radio" name="gallery_tag" value="<?= $gallery_tag->slug; ?>" id="<?= $gallery_tag->slug; ?>" class="ui-button--radio" />
                <label for="<?= $gallery_tag->slug; ?>" class="ui-button ui-button--default"><?= $gallery_tag->name; ?></label>
                <?php
            }
            ?>

            <input type="hidden" name="offset" class="gallery__offset" value="<?= $offset; ?>" />
        </form>
    </div>

    <div class="gallery__list">
        <?php
        global $post;
        $gallery = get_post_gallery($post, false);
        $gallery_items_ids = array_slice(explode(',', $gallery['ids']), 0, $offset);

        foreach ($gallery_items_ids as $attachment_id) {
            $post = get_post($attachment_id);
            setup_postdata($post);

            include(locate_template('templates/gallery/content-gallery-item.php'));
        }
        wp_reset_postdata();

        get_template_part('templates/loader');
        ?>
    </div>

    <?php get_template_part('templates/photoswipe'); ?>
</div>
