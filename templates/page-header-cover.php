<?php
$featuredImage = wp_get_attachment_image_src(get_post_thumbnail_id(), 'max');
?>

<div class="page-header__cover" style="background-image: url(<?= $featuredImage[0]; ?>);">
    <?php if (get_field('tagline')) { ?>
        <div class="page-header__tagline"><?= get_field('tagline'); ?></div>
    <?php } ?>
</div>
