<?php

namespace Roots\Sage\Setup;

use Roots\Sage\Assets;

/**
 * Theme setup
 */
function setup() {
    // Enable features from Soil when plugin is activated
    // https://roots.io/plugins/soil/
    add_theme_support('soil-clean-up');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-jquery-cdn');
    add_theme_support('soil-relative-urls');

    // Make theme available for translation
    // Community translations can be found at https://github.com/roots/sage-translations
    load_theme_textdomain('sage', get_template_directory() . '/lang');

    // Enable plugins to manage the document title
    // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
    add_theme_support('title-tag');

    // Register wp_nav_menu() menus
    // http://codex.wordpress.org/Function_Reference/register_nav_menus
    register_nav_menus([
        'primary_navigation' => __('Primary Navigation', 'sage')
    ]);

    // Enable post thumbnails
    // http://codex.wordpress.org/Post_Thumbnails
    // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
    // http://codex.wordpress.org/Function_Reference/add_image_size
    add_theme_support('post-thumbnails');
    add_image_size('max', 1600, 900);

    // Enable post formats
    // http://codex.wordpress.org/Post_Formats
    add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

    // Enable HTML5 markup support
    // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    // Use main stylesheet for visual editor
    // To add custom styles edit /assets/styles/layouts/_tinymce.scss
    add_editor_style(Assets\asset_path('styles/main.css'));

    // hide the frontend admin bar
    show_admin_bar(false);
}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

/**
 * Add current-menu-item class if URL contains the current post type's slug.
 */
function add_current_nav_class($classes, $item) {
    global $post;

    if ($post) {
        $current_post_type = get_post_type_object(get_post_type($post->ID));
        $current_post_type_slug = $current_post_type->rewrite['slug'];

        $menu_slug = strtolower(trim($item->url));

        if (strpos($menu_slug, $current_post_type_slug) !== false) {
           $classes[] = 'current-menu-item';
        }
    }

    return $classes;
}
add_filter('nav_menu_css_class', __NAMESPACE__ . '\\add_current_nav_class', 10, 2);

/**
 * Add ACF options page.
 */
if (function_exists('acf_add_options_page')) {
    acf_add_options_page([
        'page_title' => 'Options',
        'position' => 23,
    ]);
}

/**
 * Runs on theme activation.
 */
function theme_activation() {
    add_role('buyer', 'Buyer');
    update_option('default_role', 'buyer');

    add_role('support', 'Support', [
        'read' => true,
        'edit_users' => true,
        'delete_users' => true,
        'create_users' => true,
        'list_users' => true
    ]);

    if (get_role('backwpup_helper')) {
        remove_role('backwpup_helper');
    }

    if (get_role('backwpup_check')) {
        remove_role('backwpup_check');
    }

    if (get_role('backwpup_admin')) {
        remove_role('backwpup_admin');
    }
}
add_action('after_switch_theme', __NAMESPACE__ . '\\theme_activation');

/**
 * Runs on theme deactivation.
 */
function theme_deactivation() {
    if (get_role('buyer')) {
        remove_role('buyer');
    }

    if (get_role('support')) {
        remove_role('support');
    }
}
add_action('switch_theme', __NAMESPACE__ . '\\theme_deactivation');

/**
 * Theme assets
 */
function assets() {
    wp_enqueue_style('sage/css', Assets\asset_path('styles/main.css'), false, null);

    if (is_single() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }

    wp_enqueue_script('sage/js', Assets\asset_path('scripts/main.js'), ['jquery'], null, true);
    wp_localize_script('sage/js', 'ajax', ['url' => admin_url('admin-ajax.php')]);

    if (is_page('locations')) {
        wp_enqueue_script('google/maps', 'https://maps.googleapis.com/maps/api/js?key=' . GOOGLE_API_KEY . '&callback=initLocationsMap', [], null, true);
    }
}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);

/**
 * Insert code into header via wp_head.
 */
add_action('wp_head', function() {
    if (strpos($_SERVER['HTTP_HOST'], 'pattersonhomes.com') !== false) {
        ?>
        <!-- Pingdom -->
        <script>
        var _prum = [['id', '58cad753e530ca3ab94c0c79'],
                     ['mark', 'firstbyte', (new Date()).getTime()]];
        (function() {
            var s = document.getElementsByTagName('script')[0]
              , p = document.createElement('script');
            p.async = 'async';
            p.src = '//rum-static.pingdom.net/prum.min.js';
            s.parentNode.insertBefore(p, s);
        })();
        </script>
        <!-- End Pingdom -->
        <?php
        if (! is_user_logged_in()) {
            ?>
            <!-- Google Analytics -->
            <script>
            window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
            ga('create', 'UA-97336886-1', 'auto');
            ga('send', 'pageview');
            </script>
            <script async src='https://www.google-analytics.com/analytics.js'></script>
            <!-- End Google Analytics -->
            <?php
        }
    }
});
