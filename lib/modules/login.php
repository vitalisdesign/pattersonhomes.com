<?php
namespace Roots\Sage\Login;

use WP_Error;
use Roots\Sage\Email;

/**
 * Login user via AJAX.
 */
function login_user() {
    if (wp_verify_nonce($_POST['login_user_nonce'], 'login_user')) {
        $errors = new WP_Error();
        
        $response['redirect_to'] = $_POST['redirect_to'];
        
        if (empty($_POST['email'])) {
            $errors->add('email_empty', __('Please enter your email.'), ['field' => 'email']);
        }
        
        if (empty($_POST['password'])) {
            $errors->add('password_empty', __('Please enter your password.'), ['field' => 'password']);
        }
        
        if (! $errors->get_error_code()) {
            $creds['user_login'] = trim($_POST['email']);
            $creds['user_password'] = trim($_POST['password']);
            $creds['remember'] = true;
            
            // let user login with their email
            $user = get_user_by('email', $creds['user_login']);
            if (! empty($user->user_login)) {
                $creds['user_login'] = $user->user_login;
            }

            // check that the buyer's account is still active (ignore for other users)
            if (user_can($user, 'buyer')) {
                $account_created = get_user_meta($user->ID, 'account_created', true);
            } else {
                $account_created = current_time('Y-m-d');
            }

            $buyer_account_lifespan = get_field('buyer_account_lifespan', 'option');
            $account_expired = date('Y-m-d', strtotime($account_created . ' + ' . $buyer_account_lifespan . ' days'));

            if ($account_expired >= current_time('Y-m-d')) {
                $user_signon = wp_signon($creds);
                if (is_wp_error($user_signon)) {
                    $errors->add('invalid_email_or_password', __('Sorry, wrong email or password.'), ['field' => 'password']);
                }
            } else {
                $errors->add('account_expired', __('Sorry, your 1-year warranty has been fulfilled as of ' . date('F jS, Y', strtotime($account_expired)) . '.'), ['field' => 'feedback-error']);
            }
        }
        
        if ($errors->get_error_code()) {
            $response['errors'] = get_error_data($errors);
            $response['ok'] = false;
        } else {
            $response['ok'] = true;
        }
        
        echo json_encode($response);
        exit();
    } else {
        die(__('Naughty behavior detected!'));
    }
}
add_action('wp_ajax_login_user', __NAMESPACE__ . '\\login_user');
add_action('wp_ajax_nopriv_login_user', __NAMESPACE__ . '\\login_user');

/**
 * Send password reset link via AJAX.
 */
function send_password_reset_link() {
    if (wp_verify_nonce($_POST['send_password_reset_link_nonce'], 'send_password_reset_link')) {
        $errors = new WP_Error();

        if (empty($_POST['email'])) {
            $errors->add('email_empty', __('Please enter your email.'), ['field' => 'email']);
        }

        if (! $errors->get_error_code()) {
            $user = get_user_by('email', trim($_POST['email']));
            
            if (! $user) {
                $errors->add('password_reset_email_invalid', __('There is no user registered with that email.'), ['field' => 'email']);
            }
             
            if (! $errors->get_error_code()) {
                $key = get_password_reset_key($user);

                $reset_link = get_permalink(get_page_by_path('warranty-request')) . '?action=reset-password&key=' . $key . '&login=' . rawurlencode($user->user_login);
                ob_start(); ?>
                
                <p><?php printf(__('Hi %s,'), $user->first_name); ?></p>
                
                <p><?php printf(__('Someone requested that the password be reset for the %s account with the email login %s.'), get_bloginfo('name'), $user->user_email); ?></p>
                
                <p><?php _e('If this was a mistake, simply ignore this email and nothing will happen.'); ?></p>
                
                <p><?php _e('To reset your password, click on the following link:'); ?></p>

                <p><?php printf(__('<a href="%s" target="_blank">%s</a>'), $reset_link, $reset_link); ?></p>
                
                <?php
                $content = ob_get_clean();
                
                Email\send($user->user_email, __('Resetting Your Password'), $content);

                $response['message'] = __('We\'ve just sent you an email with instructions for resetting your password, so check that inbox!');
            }
        }

        if ($errors->get_error_code()) {
            $response['errors'] = get_error_data($errors);
            $response['ok'] = false;
        } else {
            $response['ok'] = true;
        }
        
        echo json_encode($response);
        exit();
    } else {
        die(__('Naughty behavior detected!'));
    }
}
add_action('wp_ajax_send_password_reset_link', __NAMESPACE__ . '\\send_password_reset_link');
add_action('wp_ajax_nopriv_send_password_reset_link', __NAMESPACE__ . '\\send_password_reset_link');

/**
 * Reset user's password via AJAX.
 */
function reset_password() {
    if (wp_verify_nonce($_POST['reset_password_nonce'], 'reset_password')) {
        $errors = new WP_Error();

        if (empty($_POST['new_password'])) {
            $errors->add('new_password_empty', __('Please enter your new password.'), ['field' => 'new-password']);
        }

        if (empty($_POST['confirm_password'])) {
            $errors->add('confirm_password_empty', __('Please confirm your new password.'), ['field' => 'confirm-password']);
        }

        if ((! empty($_POST['new_password']) && ! empty($_POST['confirm_password'])) && $_POST['new_password'] != $_POST['confirm_password']) {
            $errors->add('passwords_do_not_match', __('The passwords you entered do not match.'), ['field' => 'confirm-password']);
        }

        if (! $errors->get_error_code()) {
            wp_set_password($_POST['new_password'], $_POST['user_id']);

            $login_link = '<a href="' . get_permalink(get_page_by_path('warranty-request')) . '">Log in to your account &rarr;</a>';

            $response['message'] = sprintf(__('Your password has been changed! You can now log in to your %s account with your new password. <br /><br />%s'), get_bloginfo('name'), $login_link);
        }

        if ($errors->get_error_code()) {
            $response['errors'] = get_error_data($errors);
            $response['ok'] = false;
        } else {
            $response['ok'] = true;
        }
        
        echo json_encode($response);
        exit();
    } else {
        die(__('Naughty behavior detected!'));
    }
}
add_action('wp_ajax_reset_password', __NAMESPACE__ . '\\reset_password');
add_action('wp_ajax_nopriv_reset_password', __NAMESPACE__ . '\\reset_password');

/**
 * Get error data based on WP_Error object.
 * 
 * @param  WP_Error $errors WP_Error object.
 * @return array Errors formatted as an array.
 */
function get_error_data($errors) {
    $error_data = [];
    foreach ($errors->get_error_codes() as $error_code) {
        $error_data[] = [
            'message' => $errors->get_error_message($error_code),
            'field' => $errors->get_error_data($error_code)['field']
        ];
    }

    return $error_data;
}

/**
 * Logout user when specified URL is browsed to.
 */
function logout() {
    $url_path = trim(parse_url(add_query_arg([]), PHP_URL_PATH), '/');
    if ($url_path === 'logout') {
        wp_logout();
        exit(wp_redirect(home_url()));
    }
}
add_action('init', __NAMESPACE__ . '\\logout');
