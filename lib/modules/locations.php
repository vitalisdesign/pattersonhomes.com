<?php
namespace Roots\Sage\Locations;

use WP_Query;
use Roots\Sage\Assets;

/**
 * Register location custom post type and city taxonomy.
 */
function register_custom_post_type() {
    register_post_type('location', [
        'labels' => [
            'name' => __('Locations'),
            'singular_name' => __('Location'),
            'add_new_item' => __('Add New Location'),
            'edit_item' => __('Edit Location'),
            'new_item' => __('New Location'),
            'view_item' => __('View Location'),
            'view_items' => __('View Locations'),
            'search_items' => __('Search Locations'),
            'not_found' => __('No locations found.'),
            'not_found_in_trash' => __('No locations found in trash.'),
            'all_items' => __('All Locations'),
            'archives' => __('Location Archives'),
            'attributes' => __('Location Attributes'),
            'insert_into_item' => __('Insert into location'),
            'uploaded_to_this_item' => __('Uploaded to this location')
        ],
        'public' => true,
        'menu_icon' => 'dashicons-location-alt',
        'menu_position' => 20,
        'rewrite' => ['slug' => 'locations'],
        'hierarchical' => true,
        'supports' => ['title', 'thumbnail'],
        'taxonomies' => ['city']
    ]);

    register_taxonomy('city', ['location', 'quick_move_in'], [
        'labels' => [
            'name' => __('Cities'),
            'singular_name' => __('City'),
            'all_items' => __('All Cities'),
            'edit_item' => __('Edit City'),
            'view_item' => __('View City'),
            'update_item' => __('Update City'),
            'add_new_item' => __('Add New City'),
            'new_item_name' => __('New City Name'),
            'search_items' => __('Search Cities'),
            'popular_items' => __('Popular Cities'),
            'separate_items_with_commas' => __(''),
            'add_or_remove_items' => __('Add or remove cities'),
            'choose_from_most_used' => __(''),
            'not_found' => __('No cities found.')
        ],
        'show_admin_column' => true
    ]);
}
add_action('init', __NAMESPACE__ . '\\register_custom_post_type');

/**
 * Set Google API key for Advanced Custom Fields plugin.
 */
add_action('acf/init', function() {
    acf_update_setting('google_api_key', GOOGLE_API_KEY);
});

/**
 * Get location data via AJAX.
 */
function get_locations() {
    // get cities
    $city_terms = get_terms([
        'taxonomy' => 'city'
    ]);

    $cities = [];
    foreach ($city_terms as $city_term) {
        $city_address = get_field('address', $city_term);

        $cities[] = [
            'name' => $city_term->name,
            'lat' => $city_address['lat'],
            'lng' => $city_address['lng'],
            'icon' => Assets\asset_path('images/icons/city_marker.svg')
        ];
    }

    // get communites
    $query = new WP_Query([
        'post_type' => 'location',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'order_by' => 'menu_order',
        'order' => 'ASC'
    ]);

    $communities = [];
    while ($query->have_posts()) : $query->the_post();
        $address = get_field('address');

        $communities[] = [
            'name' => get_the_title(),
            'lat' => $address['lat'],
            'lng' => $address['lng'],
            'address' => $address['address'],
            'addressLabel' => get_field('address_label'),
            'link' => get_permalink(),
            'icon' => Assets\asset_path('images/icons/community_marker.svg')
        ];
    endwhile; wp_reset_query();

    $response = [
        'cities' => $cities,
        'communities' => $communities
    ];

    echo json_encode($response);
    exit();
}
add_action('wp_ajax_get_locations', __NAMESPACE__ . '\\get_locations');
add_action('wp_ajax_nopriv_get_locations', __NAMESPACE__ . '\\get_locations');
