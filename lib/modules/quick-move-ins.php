<?php
namespace Roots\Sage\QuickMoveIns;

use WP_Query;

/**
 * Register quick_move_in custom post type and home_type taxonomy.
 */
function register_custom_post_type() {
    register_post_type('quick_move_in', [
        'labels' => [
            'name' => __('Quick Move-ins'),
            'singular_name' => __('Quick Move-in'),
            'add_new_item' => __('Add New Quick Move-in'),
            'edit_item' => __('Edit Quick Move-in'),
            'new_item' => __('New Quick Move-in'),
            'view_item' => __('View Quick Move-in'),
            'view_items' => __('View Quick Move-ins'),
            'search_items' => __('Search Quick Move-ins'),
            'not_found' => __('No quick move-ins found.'),
            'not_found_in_trash' => __('No quick move-ins found in trash.'),
            'all_items' => __('All Quick Move-ins'),
            'archives' => __('Quick Move-in Archives'),
            'attributes' => __('Quick Move-in Attributes'),
            'insert_into_item' => __('Insert into quick move-in'),
            'uploaded_to_this_item' => __('Uploaded to this quick move-in')
        ],
        'public' => true,
        'menu_icon' => 'dashicons-admin-multisite',
        'menu_position' => 21,
        'rewrite' => ['slug' => 'quick-move-ins'],
        'supports' => ['title', 'thumbnail'],
        'taxonomies' => ['city', 'home_type']
    ]);

    register_taxonomy('home_type', ['quick_move_in'], [
        'labels' => [
            'name' => __('Home Types'),
            'singular_name' => __('Home Type'),
            'all_items' => __('All Home Types'),
            'edit_item' => __('Edit Home Type'),
            'view_item' => __('View Home Type'),
            'update_item' => __('Update Home Type'),
            'add_new_item' => __('Add New Home Type'),
            'new_item_name' => __('New Home Type Name'),
            'search_items' => __('Search Home Types'),
            'popular_items' => __('Popular Home Types'),
            'separate_items_with_commas' => __(''),
            'add_or_remove_items' => __('Add or remove home types'),
            'choose_from_most_used' => __(''),
            'not_found' => __('No home types found.')
        ],
        'show_admin_column' => true
    ]);
}
add_action('init', __NAMESPACE__ . '\\register_custom_post_type');

/**
 * Get quick move-ins via AJAX.
 */
function get_quick_move_ins() {
    $meta_query = [];
    if (!empty($_GET['city'])) {
        $city_term = get_term_by('slug', $_GET['city'], 'city');

        $meta_query[] = [
            'key' => 'city',
            'value' => $city_term->term_id
        ];
    }

    if (!empty($_GET['price'])) {
        $priceRange = explode('-', $_GET['price']);

        $meta_query[] = [
            'key' => 'price',
            'value' => $priceRange,
            'compare' => 'BETWEEN',
            'type' => 'NUMERIC'
        ];
    }

    if (!empty($_GET['home_type'])) {
        $home_type_term = get_term_by('slug', $_GET['home_type'], 'home_type');

        $meta_query[] = [
            'key' => 'home_type',
            'value' => '"' . $home_type_term->term_id . '"',
            'compare' => 'LIKE'
        ];
    }

    $query = new WP_Query([
        'post_type' => 'quick_move_in', 
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'meta_query' => $meta_query
    ]);

    ob_start();

    if ($query->have_posts()) :
        while ($query->have_posts()) : $query->the_post();
            get_template_part('templates/quick-move-ins/content', 'quick-move-ins-item');
        endwhile; wp_reset_query();
    else :
        get_template_part('templates/quick-move-ins/content', 'no-results');
    endif;

    get_template_part('templates/loader');

    $response['html'] = ob_get_clean();

    echo json_encode($response);
    exit();
}
add_action('wp_ajax_get_quick_move_ins', __NAMESPACE__ . '\\get_quick_move_ins');
add_action('wp_ajax_nopriv_get_quick_move_ins', __NAMESPACE__ . '\\get_quick_move_ins');
