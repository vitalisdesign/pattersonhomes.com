<?php
namespace Roots\Sage\Forms;

use DOMDocument;
use Roots\Sage\Assets;

/**
 * Customizes the Gravity Forms submit button HTML.
 */
function form_submit_button($button, $form) {
    $dom = new DOMDocument();
    $dom->loadHTML( $button );
    $input = $dom->getElementsByTagName( 'input' )->item(0);

    ob_start(); 
    ?>
    <button class="ui-button ui-button--primary" id="gform_submit_button_<?= $form['id']; ?>"><?= $input->getAttribute( 'value' ); ?></button>
    <?php
    return ob_get_clean();
}
add_filter('gform_submit_button', __NAMESPACE__ . '\\form_submit_button', 10, 2);

/**
 * Removes the Gravity Forms anchor element.
 */
add_filter('gform_confirmation_anchor', '__return_false');

/**
 * Customizes the Gravity Forms spinner image URL. 
 */
function spinner_url($image_src, $form) {
    return Assets\asset_path('images/icons/ajax_spinner.gif');
}
add_filter('gform_ajax_spinner_url', __NAMESPACE__ . '\\spinner_url', 10, 2);

/**
 * Populates Gravity Forms fields based on saved user meta data.
 */
function populate_fields($value, $field, $name) {
    if (is_user_logged_in()) {
        $user = wp_get_current_user();
        $user_meta = get_user_meta($user->ID);

        $values = [
            'first_name' => $user_meta['first_name'][0],
            'last_name' => $user_meta['last_name'][0],
            'email' => $user->user_email
        ];

        return isset($values[$name]) ? $values[$name] : $value;
    }
}
add_filter('gform_field_value', __NAMESPACE__ . '\\populate_fields', 10, 3);
