<?php
namespace Roots\Sage\Users;

use Roots\Sage\Email;
use Roots\Sage\Assets;

/**
 * Add custom user meta fields to Users admin section.
 */
function register_profile_fields($user) {
    if (! current_user_can('edit_users')) {
        return false;
    }
    ?>
    <table class="form-table">
        <tr>
            <th><label for="account_created">Account Creation Date</label></th>
            <?php
            if ($user == 'add-new-user') {
                $value = current_time('m/d/Y');
            } else {
                $value = get_user_meta($user->ID, 'account_created', true);
            }
            ?>
            <td><input type="text" class="datepicker regular-text" name="account_created" id="account_created" value="<?= $value; ?>" /></td>
        </tr>
    </table>

    <script>
        const buyer = document.querySelector('form[name=createuser] select[name=role]')
        const sendUserNotification = document.querySelector('form[name=createuser] input[name=send_user_notification]')
        const sendUserNotificationRow = sendUserNotification.parentElement.parentElement

        setSendUserNotification()

        buyer.addEventListener('change', (e) => setSendUserNotification())

        function setSendUserNotification() {
            if (buyer.value === 'buyer') {
                sendUserNotification.checked = false
                sendUserNotificationRow.style.display = 'none'
            } else {
                sendUserNotification.checked = true
                sendUserNotificationRow.style.display = 'table-row'
            }
        }
    </script>
    <?php
}
add_action('user_new_form', __NAMESPACE__ . '\\register_profile_fields');
add_action('show_user_profile', __NAMESPACE__ . '\\register_profile_fields');
add_action('edit_user_profile', __NAMESPACE__ . '\\register_profile_fields');

/**
 * Save custom user meta fields.
 */
function save_profile_fields($user_id) {
    if (! current_user_can('edit_users', $user_id)) {
        return false;
    }

    update_user_meta($user_id, 'account_created', $_POST['account_created']);
}
add_action('user_register', __NAMESPACE__ . '\\save_profile_fields');
add_action('personal_options_update', __NAMESPACE__ . '\\save_profile_fields');
add_action('edit_user_profile_update', __NAMESPACE__ . '\\save_profile_fields');

/**
 * Enqueue admin scripts.
 */
function enqueue_admin_scripts() {
    wp_enqueue_script('admin/js',
        Assets\asset_path('scripts/admin.js'),
        ['jquery', 'jquery-ui-core', 'jquery-ui-datepicker'],
        null,
        true
    );

    wp_enqueue_style('jquery-ui-css', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
}
add_action('admin_enqueue_scripts', __NAMESPACE__ . '\\enqueue_admin_scripts');

/**
 * Send out email notification when a new buyer account is created
 */
add_action('user_register', function($user_id) {
    if (! user_can($user_id, 'buyer')) {
        return;
    }

    $user = get_user_by('id', $user_id);
    $key = get_password_reset_key($user);

    $warranty_page_permalink = get_permalink(get_page_by_path('warranty-request'));
    $reset_link = $warranty_page_permalink . '?action=reset-password&key=' . $key . '&login=' . rawurlencode($user->user_login);

    ob_start();
    ?>

    <p>Hello,</p>

    <p>Congratulations on the purchase of your new home!</p>

    <p>Below is the login information for your warranty account with Patterson Homes. You will use this online account portal to submit any issues that may arise to be reviewed for warranty eligibility and coverage. All incoming warranty requests are monitored 24/7 by our team in case of emergent situations. For documentation purposes everything warranty related is done through your online account or through our email (<a href="mailto:pattersonhomeswarranty@gmail.com">PattersonHomesWarranty@gmail.com</a>).</p>

    <p>Please take 10-15 minutes to visit the link below to finalize your account and familiarize yourself with the warranty submission process. Stated on the left hand side of your account are the instructions for submitting requests, completion timelines, Warranty FAQ -- as well as the NASA Construction Standards document that states what is/isn't covered and a link to our recommended homeowner maintenance schedule. If you have further questions don't hesitate to reach out to our team!</p>

    <p><a href="<?= $warranty_page_permalink; ?>" target="_blank"><?= $warranty_page_permalink; ?></a></p>

    <p>Login: <?= $user->user_email; ?></p>

    <p>Before you can access the Warranty Request page above, you will want to create a login password. To set your password, click on the following link:</p>

    <p><?php printf(__('<a href="%s" target="_blank">%s</a>'), $reset_link, $reset_link); ?></p>

    <p>We are happy that you chose to build your home with Patterson Homes and look forward to assisting you resolve any issues that may arise!</p>

    <?php
    $content = ob_get_clean();
    $headers = [
        'From:pattersonhomeswarranty@gmail.com'
    ];

    Email\send($user->user_email, __('Your Patterson Homes Warranty Account'), $content, $headers);
});
