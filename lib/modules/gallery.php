<?php
namespace Roots\Sage\Gallery;

/**
 * Register gallery_tag taxonomy.
 */
function register_custom_taxonomy() {
    register_taxonomy('gallery_tag', 'attachment', [
        'labels' => [
            'name' => __('Gallery Tags'),
            'singular_name' => __('Gallery Tag'),
            'all_items' => __('All Gallery Tags'),
            'edit_item' => __('Edit Gallery Tag'),
            'view_item' => __('View Gallery Tag'),
            'update_item' => __('Update Gallery Tag'),
            'add_new_item' => __('Add New Gallery Tag'),
            'new_item_name' => __('New Gallery Tag Name'),
            'search_items' => __('Search Gallery Tags'),
            'popular_items' => __('Popular Gallery Tags'),
            'separate_items_with_commas' => __(''),
            'add_or_remove_items' => __('Add or remove gallery tags'),
            'choose_from_most_used' => __(''),
            'not_found' => __('No gallery tags found.')
        ],
        'show_ui' => false
    ]);
}
add_action('init', __NAMESPACE__ . '\\register_custom_taxonomy');

/**
 * Get gallery images via AJAX.
 */
function get_gallery_images() {
    $images_returned = 10;

    $gallery_tag = false;
    if (!empty($_GET['gallery_tag']) && $_GET['gallery_tag'] != 'all') {
        $gallery_tag = get_term_by('slug', $_GET['gallery_tag'], 'gallery_tag');
    }

    global $post;
    $gallery = get_post_gallery(get_page_by_path('gallery'), false);
    $gallery_items_ids = explode(',', $gallery['ids']);

    $filtered_gallery_items_ids =  [];
    if ($gallery_tag) {
        foreach ($gallery_items_ids as $attachment_id) {
            $attachment = get_post($attachment_id);
            if (has_term($gallery_tag->term_id, 'gallery_tag', $attachment)) {
                $filtered_gallery_items_ids[] = $attachment_id;
            }
        }
    } else {
        $filtered_gallery_items_ids = $gallery_items_ids;
    }

    if (!empty($_GET['offset'])) {
        $filtered_gallery_items_ids = array_slice($filtered_gallery_items_ids, $_GET['offset'], $images_returned);
    } else {
        $filtered_gallery_items_ids = array_slice($filtered_gallery_items_ids, 0, $images_returned);
    }

    ob_start();

    foreach ($filtered_gallery_items_ids as $attachment_id) {
        $post = get_post($attachment_id);
        setup_postdata($post);

        include(locate_template('templates/gallery/content-gallery-item.php'));
    }
    wp_reset_postdata();

    if (empty($_GET['offset'])) {
        get_template_part('templates/loader');
    }

    $response['html'] = ob_get_clean();

    echo json_encode($response);
    exit();
}
add_action('wp_ajax_get_gallery_images', __NAMESPACE__ . '\\get_gallery_images');
add_action('wp_ajax_nopriv_get_gallery_images', __NAMESPACE__ . '\\get_gallery_images');
