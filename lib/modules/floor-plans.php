<?php
namespace Roots\Sage\FloorPlans;

use WP_Query;

/**
 * Register floor_plan custom post type.
 */
function register_custom_post_type() {
    register_post_type('floor_plan', [
        'labels' => [
            'name' => __('Floor Plans'),
            'singular_name' => __('Floor Plan'),
            'add_new_item' => __('Add New Floor Plan'),
            'edit_item' => __('Edit Floor Plan'),
            'new_item' => __('New Floor Plan'),
            'view_item' => __('View Floor Plan'),
            'view_items' => __('View Floor Plans'),
            'search_items' => __('Search Floor Plans'),
            'not_found' => __('No Floor Plans found.'),
            'not_found_in_trash' => __('No floor plans found in trash.'),
            'all_items' => __('All Floor Plans'),
            'archives' => __('Floor Plan Archives'),
            'attributes' => __('Floor Plan Attributes'),
            'insert_into_item' => __('Insert into floor plan'),
            'uploaded_to_this_item' => __('Uploaded to this floor plan')
        ],
        'public' => true,
        'menu_icon' => 'dashicons-index-card',
        'menu_position' => 22,
        'rewrite' => ['slug' => 'floor-plans'],
        'hierarchical' => true,
        'supports' => ['title', 'thumbnail'],
        'taxonomies' => ['home_type']
    ]);
}
add_action('init', __NAMESPACE__ . '\\register_custom_post_type');

/**
 * Get floor plans via AJAX.
 */
function get_floor_plans() {
    $meta_query = [];
    if (!empty($_GET['home_type']) && $_GET['home_type'] != 'all') {
        $home_type_term = get_term_by('slug', $_GET['home_type'], 'home_type');

        $meta_query[] = [
            'key' => 'home_type',
            'value' => '"' . $home_type_term->term_id . '"',
            'compare' => 'LIKE'
        ];
    }

    $query = new WP_Query([
        'post_type' => 'floor_plan',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'orderby' => 'meta_value_num',
        'meta_key' => 'sq_ft',
        'meta_query' => $meta_query
    ]);

    ob_start();

    if ($query->have_posts()) :
        while ($query->have_posts()) : $query->the_post();
            get_template_part('templates/floor-plans/content', 'floor-plans-item');
        endwhile; wp_reset_query();
    else :
        get_template_part('templates/content', 'no-results');
    endif;

    get_template_part('templates/loader');

    $response['html'] = ob_get_clean();

    echo json_encode($response);
    exit();
}
add_action('wp_ajax_get_floor_plans', __NAMESPACE__ . '\\get_floor_plans');
add_action('wp_ajax_nopriv_get_floor_plans', __NAMESPACE__ . '\\get_floor_plans');
