<?php
namespace Roots\Sage\Email;

/**
 * Sets the mail content type to "text/html" and sends mail via wp_mail.
 *
 * @param  string $to      Email address to send message to.
 * @param  string $subject Email subject.
 * @param  string $message Message contents.
 * @param  string $headers Email headers.
 * @return bool Whether the email contents were sent successfully.
 */
function send($to, $subject, $message, $headers = []) {
    add_filter('wp_mail_content_type', __NAMESPACE__ . '\\mail_content_type');

    $result = wp_mail($to, $subject, $message, $headers);

    remove_filter('wp_mail_content_type', __NAMESPACE__ . '\\mail_content_type');

    return $result;
}

/**
 * Changes the email content type to HTML.
 */
function mail_content_type() {
    return 'text/html';
}

/**
 * Customizes the mail from field.
 */
function mail_from() {
    return get_option('admin_email');
}
add_filter('wp_mail_from', __NAMESPACE__ . '\\mail_from');

/**
 * Customizes the mail from name field.
 */
function mail_from_name() {
    return get_bloginfo('name');
}
add_filter('wp_mail_from_name', __NAMESPACE__ . '\\mail_from_name');
