<?php
define('GOOGLE_API_KEY', 'AIzaSyAelDI5ubvFNOwPOYkCCaTYxyV8mTokEsU');
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
    'lib/assets.php',    // Scripts and stylesheets
    'lib/extras.php',    // Custom functions
    'lib/setup.php',     // Theme setup
    'lib/titles.php',    // Page titles
    'lib/wrapper.php',   // Theme wrapper class
    'lib/customizer.php', // Theme customizer
    'lib/modules/locations.php',
    'lib/modules/quick-move-ins.php',
    'lib/modules/floor-plans.php',
    'lib/modules/gallery.php',
    'lib/modules/forms.php',
    'lib/modules/login.php',
    'lib/modules/email.php',
    'lib/modules/users.php'
];

foreach ($sage_includes as $file) {
    if (!$filepath = locate_template($file)) {
        trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
    }

    require_once $filepath;
}
unset($file, $filepath);
