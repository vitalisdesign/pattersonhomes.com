/**
 * Re-initialize Select2 on form select elements.
 */
jQuery(document).bind('gform_post_render', function() {
    initSelect2();
});
