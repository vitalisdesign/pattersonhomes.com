jQuery(document).ready(function() {
    /**
     * Update quick move-ins when filter is changed.
     */
    jQuery('.quick-move-ins__filter :input').change(function() {
        var form = jQuery('.quick-move-ins__filter');
        var data = jQuery(form).serializeArray();
        data.push({ 
            'name': 'action',
            'value': 'get_quick_move_ins'
        });
        
        jQuery.ajax({
            type: 'GET',
            url: ajax.url,
            data: data,
            dataType: 'json',
            beforeSend: function() {
                jQuery('.loader').css({'display': 'flex'});
            },
            success: function(response) {
                jQuery('.quick-move-ins__list').html(response.html);
            },
            error: function(response) {
                console.log(response);
            },
            complete: function() {
                jQuery('.loader').hide();
            }
        });

        jQuery('.quick-move-ins__filter__clear').css({'display': 'flex'}); 
    });

    /**
     * Clear quick move-ins filter when button is clicked.
     */
    jQuery('.quick-move-ins__filter__clear').click(function() {
        jQuery('.quick-move-ins__filter :input').val(null).trigger('change');
        jQuery(this).hide();

        return false;
    });
});
