/**
 * Initialize jQuery UI datepicker.
 */
jQuery(document).ready(function() {
    jQuery('input.datepicker').datepicker({
        dateFormat: 'mm/dd/yy'
    });
});
