/**
 * Initialize Photoswipe on the specified gallery.
 */
function initPhotoswipe(gallery) {
    jQuery(gallery).on('click', 'a', function(e) {
        e.preventDefault();

        var items = getPhotoswipeItems(gallery);
        var options = {
            index: jQuery(this).index(),
            bgOpacity: 0.9,
            getThumbBoundsFn: function(index) {
                var thumbnail = jQuery(gallery).find('a').eq(index).find('img'),
                    rect = thumbnail[0].getBoundingClientRect();

                return {
                    x: rect.left, 
                    y: rect.top + jQuery(window).scrollTop(), 
                    w: rect.width
                };
            },
            history: false
        };

        var photoswipe = new PhotoSwipe(jQuery('.pswp')[0], PhotoSwipeUI_Default, items, options);
        photoswipe.init();
    });

    /**
     * Get Photoswipe items based on current gallery images.
     */
    function getPhotoswipeItems(gallery) {
        var items = [];

        jQuery(gallery).find('a').each(function() {
            var size = jQuery(this).data('size').split('x');
            var item = {
                src: jQuery(this).attr('href'),
                w: size[0],
                h: size[1],
                msrc: jQuery(this).find('img').attr('src')
            };

            items.push(item);
        });

        return items;
    }
}
