(function() {
    /**
     * Login user via AJAX.
     */
    jQuery('#login-form').on('submit', function() {
        var form = jQuery(this);
        var formSubmit = jQuery(form).find('button[type=submit]');
        var data = jQuery(form).serializeArray();
        data.push({name: 'action', value: 'login_user'});
        
        jQuery.ajax({ 
            type: 'POST',
            url: ajax.url,
            data: data,
            dataType: 'json',
            beforeSend: function() {
                jQuery(formSubmit).attr('disabled', true).addClass('loading');
            },
            success: function(response) {
                removeFormErrors(form);

                if (response.ok) {
                    window.location = response.redirect_to;
                } else {
                    addFormErrors(form, response.errors);
                }
            },
            error: function(response) {
                console.log(response);
            },
            complete: function() {
                jQuery(formSubmit).removeAttr('disabled').removeClass('loading');
            }
        });
        
        return false;
    });

    /**
     * Send password reset link via AJAX.
     */
    jQuery('#send-password-reset-link-form').on('submit', function() {
        var form = jQuery(this);
        var formSubmit = jQuery(form).find('button[type=submit]');
        var data = jQuery(form).serializeArray();
        data.push({name: 'action', value: 'send_password_reset_link'});
        
        jQuery.ajax({ 
            type: 'POST',
            url: ajax.url,
            data: data,
            dataType: 'json',
            beforeSend: function() {
                jQuery(formSubmit).attr('disabled', true).addClass('loading');
            },
            success: function(response)  {
                removeFormErrors(form);

                if (response.ok) {
                    jQuery(form).find('.login__feedback').html(response.message).show();
                } else {
                    addFormErrors(form, response.errors);
                }
            },
            error: function(response) {
                console.log(response);
            },
            complete: function() {
                jQuery(formSubmit).removeAttr('disabled').removeClass('loading');
            }
        });
        
        return false;
    });

    /**
     * Reset user password via AJAX.
     */
    jQuery('#reset-password-form').on('submit', function() {
        var form = jQuery(this);
        var formSubmit = jQuery(form).find('button[type=submit]');
        var data = jQuery(form).serializeArray();
        data.push({name: 'action', value: 'reset_password'});
        
        jQuery.ajax({ 
            type: 'POST',
            url: ajax.url,
            data: data,
            dataType: 'json',
            beforeSend: function() {
                jQuery(formSubmit).attr('disabled', true).addClass('loading');
            },
            success: function(response) {
                removeFormErrors(form);

                if (response.ok) {
                    jQuery(form).find('.login__feedback').html(response.message).show();
                } else {
                    addFormErrors(form, response.errors);
                }
            },
            error: function(response) {
                console.log(response);
            },
            complete: function() {
                jQuery(formSubmit).removeAttr('disabled').removeClass('loading');
            }
        });
        
        return false;
    });

    /**
     * Show #send-password-reset-link-form and hide #login-form.
     */
    jQuery('#send-password-reset-link-form-link').on('click', function() {
        jQuery('#login-form').hide();
        jQuery('#send-password-reset-link-form').show();
    });

    /**
     * Show #login-form and hide #send-password-reset-link-form.
     */
    jQuery('#login-form-link').on('click', function() {
        jQuery('#login-form').show();
        jQuery('#send-password-reset-link-form').hide();
    });

    /**
     * Remove form errors.
     */
    function removeFormErrors(form) {
        jQuery(form).find('.error').removeClass('error');
        jQuery(form).find('.error-message').remove();
    }

    /**
     * Add form errors.
     */
    function addFormErrors(form, errors) {
        errors.forEach(function(error) {
            var field = jQuery(form).find('#' + error.field);
            jQuery(field).addClass('error');
            jQuery('<div class="error-message">' + error.message + '</div>').insertAfter(field);
        });
    }
})();
