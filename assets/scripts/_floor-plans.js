jQuery(document).ready(function() {
    /**
     * Update floor plans when filter is changed.
     */
    jQuery('.floor-plans__filter :input').change(function() {
        var form = jQuery('.floor-plans__filter');
        var data = jQuery(form).serializeArray();
        data.push({ 
            'name': 'action',
            'value': 'get_floor_plans'
        });
        
        jQuery.ajax({
            type: 'GET',
            url: ajax.url,
            data: data,
            dataType: 'json',
            beforeSend: function() {
                jQuery('.loader').css({'display': 'flex'});
            },
            success: function(response) {
                jQuery('.floor-plans__list').html(response.html);
            },
            error: function(response) {
                console.log(response);
            },
            complete: function() {
                jQuery('.loader').hide();
            }
        });
    });

    /**
     * Initialize Photoswipe for floor plans.
     */
    initPhotoswipe(jQuery('.floor-plan__floor-plans'));

    /**
     * Initialize Slick Carousel for floor plans.
     */
    jQuery('.floor-plan__slider').slick({
        arrows: false,
        dots: true,
        slidesToShow: 1,
        centerMode: true,
        centerPadding: '10%',
        variableWidth: true,
        mobileFirst: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    centerPadding: '10%',
                }
            },
            {
                breakpoint: 992,
                settings: {
                    arrows: true
                }
            }
        ]
    });

    /**
     * Initialize Photoswipe for image gallery.
     */
    initPhotoswipe(jQuery('.floor-plan__slider').find('.slick-track'));
});
