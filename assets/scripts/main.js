/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

    // Use this variable to set up the common and page specific functions. If you
    // rename this variable, you will also need to rename the namespace below.
    var Sage = {
        // All pages
        'common': {
            init: function() {
                toggleHeaderNav();
                initSelect2();
                initPageNav();
            },
            finalize: function() {
                // JavaScript to be fired on all pages, after page specific JS is fired
            }
        },
        // Home page
        'home': {
            init: function() {
                // JavaScript to be fired on the home page
            },
            finalize: function() {
                // JavaScript to be fired on the home page, after the init JS
            }
        },
        // About us page, note the change from about-us to about_us.
        'about_us': {
            init: function() {
                // JavaScript to be fired on the about us page
            }
        }
    };

    // The routing fires all common scripts, followed by the page specific scripts.
    // Add additional events for more control over timing e.g. a finalize event
    var UTIL = {
        fire: function(func, funcname, args) {
            var fire;
            var namespace = Sage;
            funcname = (funcname === undefined) ? 'init' : funcname;
            fire = func !== '';
            fire = fire && namespace[func];
            fire = fire && typeof namespace[func][funcname] === 'function';

            if (fire) {
                namespace[func][funcname](args);
            }
        },
        loadEvents: function() {
            // Fire common init JS
            UTIL.fire('common');

            // Fire page-specific init JS, and then finalize JS
            $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
                UTIL.fire(classnm);
                UTIL.fire(classnm, 'finalize');
            });

            // Fire common finalize JS
            UTIL.fire('common', 'finalize');
        }
    };

    // Load Events
    $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.

 /**
  * Toggle the mobile nav hamburger icon.
  */
function toggleHeaderNav() {
    jQuery('.header__toggle').click(function() {
        jQuery(this).toggleClass('header__toggle--open');
        jQuery('.header').toggleClass('header--open');
    });

    jQuery(document).on('click', function(event) {
        if (!jQuery(event.target).closest('.header').length) {
            jQuery('.header').removeClass('header--open');
            jQuery('.header__toggle').removeClass('header__toggle--open');
        }
    });
}

/**
 * Initialize Select2 on all select elements.
 */
function initSelect2() {
    jQuery('select').select2({
        placeholder: jQuery(this).attr('data-placeholder'),
        minimumResultsForSearch: Infinity
    });
}

/**
 * Initialize .pageNav on enabled pages.
 */
function initPageNav() {
    if (jQuery('.page-nav').length) {
        var header = jQuery('.header'),
            headerHeight = jQuery(header).outerHeight(),
            pageHeader = jQuery('.page-header__cover'),
            pageHeaderHeight = jQuery(pageHeader).outerHeight(),
            pageNav = jQuery('.page-nav'),
            pageNavHeight = jQuery(pageNav).outerHeight(),
            pageNavSpacer = jQuery('.page-nav__spacer'),
            pageNavItemActiveClass = 'page-nav__item--active',
            offset = headerHeight + pageNavHeight + 40;

        jQuery(window).on('scroll', onScroll);

        jQuery(pageNav).find('a').click(function(e) {
            e.preventDefault();

            // enable smooth scrolling between sections
            if (jQuery(pageNav).hasClass('page-nav--fixed')) {
                jQuery(window).off('scroll');

                jQuery(pageNav).find('a').each(function() {
                    jQuery(this).removeClass(pageNavItemActiveClass);
                });
                jQuery(this).addClass(pageNavItemActiveClass);
            }

            jQuery('html, body').animate({
                scrollTop: jQuery(jQuery(this).attr('href')).offset().top - offset + 1
            }, 250, 'swing', function() {
                jQuery(window).on('scroll', onScroll);
            });
        });
    }

    function onScroll() {
        var scrollPos = jQuery(window).scrollTop();

        if (scrollPos > (pageHeaderHeight - headerHeight)) {
            jQuery(pageNav).addClass('page-nav--fixed');
            jQuery(pageNavSpacer).show();
        } else {
            jQuery(pageNav).removeClass('page-nav--fixed');
            jQuery(pageNavSpacer).hide();
        }

        jQuery(pageNav).find('a').each(function () {
            var currLink = jQuery(this);
            var refElement = jQuery(currLink.attr('href'));

            if (scrollPos > refElement.position().top - offset && scrollPos <= refElement.position().top + refElement.height() + offset) {
                jQuery(pageNav).find('a').removeClass(pageNavItemActiveClass);
                currLink.addClass(pageNavItemActiveClass);
            } else {
                currLink.removeClass(pageNavItemActiveClass);
            }
        });
    }
}
