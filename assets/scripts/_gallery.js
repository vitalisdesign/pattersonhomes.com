(function() {
    var gallery = jQuery('.gallery__list');
    var filter = jQuery('.gallery__filter');
    var doingAjax = false;

    jQuery(document).ready(function() {
        initGallery(gallery);
        initPhotoswipe(gallery);

        /**
         * Update gallery when filter is changed.
         */
        jQuery('.gallery__filter :input').change(function() {
            updateGalleryOffset(0);

            var data = jQuery(filter).serializeArray();
            data.push({ 
                'name': 'action',
                'value': 'get_gallery_images'
            });
            
            jQuery.ajax({
                type: 'GET',
                url: ajax.url,
                data: data,
                dataType: 'json',
                beforeSend: function() {
                    jQuery('.loader').css({'display': 'flex'});
                },
                success: function(response) {                
                    jQuery(gallery).html(response.html);
                    initGallery(gallery);
                    updateGalleryOffset(false);
                },
                error: function(response) {
                    console.log(response);
                },
                complete: function() {
                    jQuery('.loader').hide();
                }
            });
        });
    });

    /**
     * Initialize Justified Gallery for gallery images.
     */
    function initGallery(gallery) {
        jQuery(gallery).justifiedGallery({
            rowHeight: 250,
            lastRow: 'justify',
            captions: false,
            margins: 4,
            selector: 'a, div:not(.loader)'
        });
    }

    /**
     * Update gallery offset based on how many images have been loaded.
     */
    function updateGalleryOffset(offset) {
        if (offset !== false) {
            jQuery(filter).find('.gallery__offset').val(0);
        } else {
            jQuery(filter).find('.gallery__offset').val(jQuery(gallery).find('a').length);
        }
    }

    /**
     * Load more gallery images on scroll, if available.
     */
    jQuery(window).scroll(function() {
        if (jQuery(gallery).length) {
            var galleryTop = jQuery(gallery).offset().top,
                galleryHeight = jQuery(gallery).outerHeight(),
                windowTop = jQuery(window).scrollTop(),
                windowHeight = jQuery(window).height();

            if (windowTop > (galleryTop + galleryHeight - windowHeight) && ! doingAjax) {
                loadMoreImages();
            }
        }
    });

    /**
     * Load more gallery images via AJAX.
     */
    function loadMoreImages() {
        var data = jQuery(filter).serializeArray();
        data.push({ 
            'name': 'action',
            'value': 'get_gallery_images'
        });

        jQuery.ajax({
            type: 'GET',
            url: ajax.url,
            data: data,
            dataType: 'json',
            beforeSend: function() {
                doingAjax = true;
            },
            success: function(response) {
                jQuery(response.html).insertBefore(jQuery(gallery).find('.loader'));
                jQuery(gallery).justifiedGallery('norewind');
                updateGalleryOffset(false);
            },
            error: function(response) {
                console.log(response);
            },
            complete: function() {
                doingAjax = false;
            }
        });
    }
})();
