var map, infowindow;
var cityMarkers = [];
var communityMarkers = [];

/**
 * Get locations map data via AJAX.
 */
function initLocationsMap() {
    jQuery.ajax({
        type: 'GET',
        url: ajax.url,
        data: {action: 'get_locations'},
        dataType: 'json',
        success: function(response) {
            initMap();
            setCityMarkers(response.cities);
            setCommunityMarkers(response.communities);
        },
        error: function(response) {
            console.log(response);
        },
    });
}

/**
 * Initialize locations map.
 */
function initMap() {
    map = new google.maps.Map(document.getElementById('locations-map'), {
        center: {lat: 40.31064500000001, lng: -111.904492},
        mapTypeControl: false,
        zoom: 10,
        styles: [{"featureType":"landscape","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"stylers":[{"hue":"#00aaff"},{"saturation":-100},{"gamma":2.15},{"lightness":12}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"lightness":24}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":57}]}]
    });

    map.addListener('zoom_changed', function() {
        infowindow.close();
        var zoom = map.getZoom();

        for (i = 0; i < cityMarkers.length; i++) {
            cityMarkers[i].setVisible(zoom <= 12);
        }

        for (i = 0; i < communityMarkers.length; i++) {
            communityMarkers[i].setVisible(zoom > 12);
        }
    });

    infowindow = new google.maps.InfoWindow();

    // style infowindow
    google.maps.event.addListener(infowindow, 'domready', function() {
        var iwOuter = jQuery('.gm-style-iw');
        var iwBackground = iwOuter.prev();

        // remove background border
        iwBackground.children(':nth-child(2)').css({'background' : 'none'});
        // remove tip shadow
        iwBackground.children(':nth-child(3)').children().children().css({'box-shadow' : 'none'});
    });
}

/**
 * Place city markers on locations map.
 */
function setCityMarkers(cities) {
    for (var i = 0; i < cities.length; i++) {
        var city = cities[i];

        var marker = new google.maps.Marker({
            title: city.name,
            position: {lat: parseFloat(city.lat), lng: parseFloat(city.lng)},
            icon: city.icon,
            map: map
        });

        marker.addListener('click', function() {
            return function() {
                infowindow.close();
                map.setCenter(this.getPosition());
                map.setZoom(13);
            };
        }());

        contentString = '<div class="map__infowindow">';
        contentString += '<div class="map__infowindow__name">' + city.name + '</div>';
        contentString += '</div>';

        marker.addListener('mouseover', function(content) {
            return function() {
                infowindow.setContent(content);
                infowindow.open(map, this);
            };
        }(contentString));

        marker.addListener('mouseout', function() {
            return function() {
                infowindow.close();
            };
        }());

        cityMarkers.push(marker);
    }
}

/**
 * Place community markers on locations map.
 */
function setCommunityMarkers(communities) {
    for (var i = 0; i < communities.length; i++) {
        var community = communities[i];

        var marker = new google.maps.Marker({
            title: community.name,
            position: {lat: parseFloat(community.lat), lng: parseFloat(community.lng)},
            icon: community.icon,
            map: map,
            visible: false
        });

        var directionsLink = 'https://www.google.com/maps/dir//' + community.address;

        var addressLabel = community.addressLabel;
        if (!addressLabel) {
            addressLabel = community.address.replace(',', '<br />');
        }

        contentString = '<div class="map__infowindow">';

        contentString += '<div class="map__infowindow__title">' + community.name + '</div>';
        contentString += '<a class="map__infowindow__address" href="' + directionsLink + '" target="_blank"><i class="material-icons">location_on</i><span>' + addressLabel + '</span></a>';

        contentString += '<div class="map__infowindow__buttons">';
        contentString += '<a class="ui-button ui-button--secondary ui-button--uppercase" href="' + community.link + '">More Info</a>';
        contentString += '<a class="ui-button ui-button--secondary ui-button--uppercase" href="' + directionsLink + '" target="_blank">Get Directions</a>';
        contentString += '</div>';

        contentString += '</div>';

        marker.addListener('mouseover', function(content) {
            return function() {
                infowindow.setContent(content);
                infowindow.open(map, this);
            };
        }(contentString));

        communityMarkers.push(marker);
    }
}
