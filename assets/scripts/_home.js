jQuery(document).ready(function() {
    /**
     * Initialize Slick Carousel for home slider.
     */
    jQuery('.home__slider').slick({
        autoplay: true,
        autoplaySpeed: 3500,
        arrows: false,
        dots: true,
        mobileFirst: true,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    arrows: true
                }
            }
        ],
        speed: 350
    });
});
